﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;

namespace Authorization.IdentityServer
{
    public static class DatabaseInitializer
    {
        public static void Init(IServiceProvider scopeServiceProvider)
        {
            var userManager = scopeServiceProvider.GetService<UserManager<IdentityUser>>();
            //dbContext.Users.RemoveRange(dbContext.Users);

            var testUser = new IdentityUser()
            {
                UserName = "qwe",
            };

            userManager.DeleteAsync(testUser).Wait();

            var result = userManager.CreateAsync(testUser, "asd").Result;
            if (result.Succeeded)
            {
                userManager.AddClaimAsync(testUser, new Claim(ClaimTypes.Role, RoleNames.Administrator)).Wait();
            }

        }
    }
}