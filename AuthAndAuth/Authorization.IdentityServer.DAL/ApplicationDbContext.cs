﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Authorization.IdentityServer.DAL
{
    public class ApplicationDbContext : IdentityDbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "User ID=postgres; Password=asdqwe; Server=localhost; Port=5432; Database=IdentityServer; Integrated Security = true; Pooling = true;");
            base.OnConfiguring(optionsBuilder);
        }
    }
}