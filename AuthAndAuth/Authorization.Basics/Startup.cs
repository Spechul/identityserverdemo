using System.Security.Claims;
using Authorization.Basics.Constants;
using Authorization.DAL;
using Authorization.DAL.Entity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Authorization.Basics
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(config =>
            {
                config.UseNpgsql(
                    "User ID=postgres; Password=asdqwe; Server=localhost; Port=5432;Database=IdentityServerTest; Integrated Security = true; Pooling = true;");
            })
                .AddIdentity<ApplicationUser, ApplicationRole>(config =>
                {
                    config.Password.RequireDigit = false;
                    config.Password.RequireUppercase = false;
                    config.Password.RequireNonAlphanumeric = false;
                    config.Password.RequireLowercase = false;
                    config.Password.RequiredLength = 3;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddControllersWithViews();

            services.ConfigureApplicationCookie(config =>
            {
                config.LoginPath = "/Admin/Login";
                config.AccessDeniedPath = "/Home/AccessDenied";
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(RoleNames.Administrator, builder =>
                {
                    builder.RequireClaim(ClaimTypes.Role, RoleNames.Administrator);
                });

                options.AddPolicy(RoleNames.Manager, builder =>
                {
                    builder.RequireAssertion(assert => 
                        assert.User.HasClaim(ClaimTypes.Role, RoleNames.Administrator) 
                        || assert.User.HasClaim(ClaimTypes.Role, RoleNames.Manager));
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapDefaultControllerRoute();
            });
        }
    }
}
