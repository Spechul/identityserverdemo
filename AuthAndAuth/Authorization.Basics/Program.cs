using System;
using System.Security.Claims;
using Authorization.Basics.Constants;
using Authorization.DAL.Entity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Authorization.Basics
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();
            using (var scope = host.Services.CreateScope())
            {
                DatabaseInitializer.Init(scope.ServiceProvider);
            }
            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }

    public static class DatabaseInitializer
    {
        public static void Init(IServiceProvider scopeServiceProvider)
        {
            var userManager = scopeServiceProvider.GetService<UserManager<ApplicationUser>>();
            //dbContext.Users.RemoveRange(dbContext.Users);
            
            var testUser = new ApplicationUser
            {
                FirstName = "qwe1",
                LastName = "qwe2",
                UserName = "qwe",
            };

            userManager.DeleteAsync(testUser).Wait();

            var result = userManager.CreateAsync(testUser, "asd").Result;
            if (result.Succeeded)
            {
                userManager.AddClaimAsync(testUser, new Claim(ClaimTypes.Role, RoleNames.Administrator)).Wait();
            }

        }
    }
}
