﻿namespace Authorization.Basics.Constants
{
    public static class RoleNames
    {
        public const string Administrator = "Administrator";
        public const string Manager = "Manager";
    }
}