﻿using System;
using Microsoft.AspNetCore.Identity;

namespace Authorization.DAL.Entity
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}