apps require postgres up in docker (or on without it).

```powershell
cd docker/AuthAndAuth
docker-compose up
```

Before first run go to package manager console and apply migrations (use DAL projects as starting ones for this) before starting.

```packagemanager
Update-Database
```

Or do it with dotnet CLI (specifying context folder of course)

```powershell
dotnet ef database update
```


### User Authentication (Code grant type)

requires running projects:
- Authorization.IdentityServer
- Authorization.Client.Mvc

### Api Authentication (ClientCredentials grant type)

requires running projects:
- Authorization.IdentityServer
- Authorization.Orders.Api
- Authorization.Users.Api