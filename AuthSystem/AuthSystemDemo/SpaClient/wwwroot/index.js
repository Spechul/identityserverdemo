﻿var config = {
    authority: "https://localhost:5005",
    client_id: "spa_client",
    response_type: "id_token token",
    redirect_uri: "https://localhost:5003/Home/SignIn",
    scope: "openid profile ApiOne ClientMvc"
}

var userManager = new Oidc.UserManager(config);

var signIn = function () {
    userManager.signinRedirect();
};

userManager.getUser().then(user => {
    if (user) {
        //axios.defaults.headers.common["Authorization"] = "Bearer " + user.access_token;
        axios.defaults.headers.common = { "Authorization": `Bearer ${user.access_token}` };
    }
});

var callApi = function () {
    userManager.getUser().then(user => {
        console.log(user.access_token);
        axios.get("https://localhost:5007/Secret/Array")
            .then(res => {
                console.log(res);
            });
    });

}
