document.getElementById("login_btn").addEventListener("click", login);

const config =
{
    authority: "https://localhost:5005",
    client_id: "auth_system_spa_client",
    response_type: "code",
    scope: "openid profile ApiOne",
    redirect_uri: "https://localhost:5003/callback.html"
}

const manager = new Oidc.UserManager(config);

manager.getUser().then(function (user) {
    if (user) {
        print("user logged in:", user);
    } else {
        print("user not logged in");
    }
});


function callApi() {
    manager.getUser().then(user => {
        if (user === null) {
            print("Unauthorized");
            return;
        }

        const xhr = new XMLHttpRequest();
        xhr.open("GET", "https://localhost:5007/Secret/Admin");
        xhr.onload = function() {
            if (xhr.status === 200) {
                print(xhr.responseText, xhr.response);
            } else {
                print("something has broken ", xhr);
            }
        };

        xhr.setRequestHeader("Authorization", "Bearer " + user.access_token);
        xhr.send();

    }).catch(error => {
        print(error);
    });
}

function login() {
    manager.signinRedirect();
}


function print(message, data) {
    if (message) {
        document.getElementById("message").innerText = message;
    }

    if (data && typeof data === "object") {
        document.getElementById("data").innerText = JSON.stringify(data, null, 2);
    }
}