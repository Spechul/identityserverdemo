const manager = new Oidc.UserManager({
    loadUserInfo: true,
    response_mode: "query"
});

manager.signinRedirectCallback()
    .then(user => {
        console.log(user);
        window.location.href = "Index.html";
    })
    .catch(error => {
        console.log(error);
    });
