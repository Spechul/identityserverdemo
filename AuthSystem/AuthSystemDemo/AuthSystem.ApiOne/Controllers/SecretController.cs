﻿using System.Collections.Generic;
using AuthSystem.DataModel.Auth;
using AuthSystem.DataModel.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthSystem.ApiOne.Controllers
{
    [Route("Secret")]
    public class SecretController : Controller
    {
        [Route("String")]
        [Authorize]
        public JsonResult String()
        {
            return new JsonResult("secret ApiOne message");
        }

        [Route(nameof(Admin))]
        [Authorize(Policy = AuthConstants.Roles.Administrator)]
        public JsonResult Admin()
        {
            return new JsonResult("admin only sees this");
        }

        [Route(nameof(Array))]
        [Authorize]
        public JsonResult Array(int amount = 5)
        {
            /*var user = User;
            bool admin = user.HasClaim(ClaimTypes.Role, AuthConstants.Roles.Administrator);*/
            List<Measurement> measurements = new List<Measurement>();
            for (int i = 0; i < 5; i++)
            {
                measurements.Add(Measurement.RandomMeasurement());
            }
            return new JsonResult(measurements);
        }
    }
}