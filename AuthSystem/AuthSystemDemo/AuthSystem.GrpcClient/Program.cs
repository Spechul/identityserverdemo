﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using AuthSystem.GrpcService;
using Grpc.Core;
using Grpc.Net.Client;
using IdentityModel.Client;

namespace AuthSystem.GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            // The port number(5001) must match the port of the gRPC server.
            Console.ReadLine();
            using var authClient = new HttpClient();
            var discoveryDocument = await authClient.GetDiscoveryDocumentAsync("https://localhost:5005");
            var tokenResponse = await authClient.RequestClientCredentialsTokenAsync(new ClientCredentialsTokenRequest()
            {
                Address = discoveryDocument.TokenEndpoint,
                ClientId = "ProtectedGrpc",
                ClientSecret = "grpc_protected_secret",
                Scope = "grpc_protected_scope"
            });

            var metadata = new Metadata
            {
                {"Authorization", $"Bearer {tokenResponse.AccessToken}"}
            };

            using var channel = GrpcChannel.ForAddress("https://localhost:5009");

            var client = new Greeter.GreeterClient(channel);
            var callOptions = new CallOptions(metadata);


            var reply = await client.SayHelloAsync(
                new HelloRequest
                {
                    Name = "GreeterClient"
                }, callOptions);
            Console.WriteLine("Greeting: " + reply.Message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
