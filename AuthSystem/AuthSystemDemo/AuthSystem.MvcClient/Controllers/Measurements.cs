﻿using System.Collections.Generic;
using System.Security.Claims;
using AuthSystem.DataModel.Auth;
using AuthSystem.DataModel.Data;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AuthSystem.MvcClient.Controllers
{
    public class Measurements : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public JsonResult Array(int amount=5)
        {
            /*var user = User;
            bool admin = user.HasClaim(ClaimTypes.Role, AuthConstants.Roles.Administrator);*/
            List<Measurement> measurements = new List<Measurement>();
            for (int i = 0; i < 5; i++)
            {
                measurements.Add(Measurement.RandomMeasurement());
            }
            return new JsonResult(measurements);
        }

        [Authorize(Policy = AuthConstants.Roles.Administrator)]
        public IActionResult Admin()
        {
            return View();
        }

        public IActionResult LogOut()
        {
            var parameters = new AuthenticationProperties() {RedirectUri = "/Measurements/Index"};
            return SignOut(
                parameters,
                CookieAuthenticationDefaults.AuthenticationScheme,
                OpenIdConnectDefaults.AuthenticationScheme);
        }
    }
}