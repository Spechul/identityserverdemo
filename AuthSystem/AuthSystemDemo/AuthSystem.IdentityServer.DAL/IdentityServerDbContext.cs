﻿using System;
using System.Linq;
using AuthSystem.DataModel.User;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AuthSystem.IdentityServer.DAL
{
    public class IdentityServerDbContext : IdentityDbContext<AuthSystemUser, AuthSystemRole, Guid>
    {
        public DbSet<Company> Companies { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Company>()
                .HasMany(c => c.Users)
                .WithOne();

            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(
                "User ID=postgres; Password=asdqwe; Server=localhost; Port=5432; Database=IdentityServer; Integrated Security = true; Pooling = true;");
            base.OnConfiguring(optionsBuilder);
        }
    }
}
