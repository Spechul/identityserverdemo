﻿using System.Collections.Generic;
using System.Security.Claims;
using AuthSystem.DataModel.Auth;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;

namespace AuthSystem.IdentityServer
{
    public static class IdentityServerConfiguration
    {
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>()
            {
                new Client()
                {
                    ClientId = "code_client",
                    ClientSecrets =
                    {
                        new Secret("code_secret".ToSha256())
                    },
                    AllowedGrantTypes = GrantTypes.Code,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                    },
                    RedirectUris =
                    {
                        "https://localhost:5001/signin-oidc"
                    },
                    PostLogoutRedirectUris =
                    {
                        "https://localhost:5001/signout-callback-oidc"
                    },
                    AlwaysIncludeUserClaimsInIdToken = true,
                },
                new Client()
                {
                    ClientId = "spa_client",
                    ClientSecrets =
                    {
                        new Secret("spa_secret".ToSha256())
                    },
                    AllowedCorsOrigins =
                    {
                        "https://localhost:5003",
                        "https://localhost:5007",
                    },
                    RedirectUris =
                    {
                        "https://localhost:5003/Home/SignIn"
                    },
                    AlwaysIncludeUserClaimsInIdToken = true,
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowAccessTokensViaBrowser = true,
                    RequireConsent = false,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        AuthConstants.ApiResources.ApiOne,
                        AuthConstants.ApiScopes.ClientMvc,
                    },
                },
                new Client()
                {
                    ClientId = "auth_system_spa_client",
                    RequireClientSecret = false,
                    RequireConsent = false,
                    RequirePkce = true,
                    AllowedGrantTypes = GrantTypes.Code,
                    RedirectUris =
                    {
                        "https://localhost:5003/callback.html"
                    },
                    AllowedCorsOrigins =
                    {
                        "https://localhost:5003"
                    },
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.Profile,
                        IdentityServerConstants.StandardScopes.OpenId,
                        AuthConstants.ApiResources.ApiOne,
                    }
                },
                new Client()
                {
                    ClientId = "ProtectedGrpc",
                    ClientName = "ProtectedGrpc",
                    ClientSecrets = new List<Secret> { new Secret { Value = "grpc_protected_secret".Sha256() } },
                    AllowedGrantTypes = GrantTypes.ClientCredentials,
                    AllowedScopes = new List<string> { "grpc_protected_scope" }
                },
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>()
            {
                new ApiResource(AuthConstants.ApiResources.ApiOne),
                new ApiResource(AuthConstants.ApiResources.Measurements),
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>()
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
            };
        }

        public static IEnumerable<ApiScope> GetApiScopes()
        {
            return new List<ApiScope>()
            {
                new ApiScope(AuthConstants.ApiScopes.ClientMvc),
                new ApiScope(ClaimTypes.Role),
                new ApiScope(AuthConstants.ApiResources.ApiOne),
                new ApiScope("grpc_protected_scope"),
            };
        }
    }
}