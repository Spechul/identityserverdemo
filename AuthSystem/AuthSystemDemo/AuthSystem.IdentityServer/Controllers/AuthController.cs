﻿using System.Threading.Tasks;
using AuthSystem.DataModel.User;
using AuthSystem.IdentityServer.ViewModels;
using IdentityServer4.Extensions;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace AuthSystem.IdentityServer.Controllers
{
    public class AuthController : Controller
    {
        private readonly UserManager<AuthSystemUser> _userManager;
        private readonly SignInManager<AuthSystemUser> _signInManager;
        private readonly IIdentityServerInteractionService _interactionService;

        public AuthController(UserManager<AuthSystemUser> userManager,
            SignInManager<AuthSystemUser> signInManager,
            IIdentityServerInteractionService interactionService)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _interactionService = interactionService;
        }

        public IActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel()
            {
                UserName = "qwe",
                Password = "asd",
                ReturnUrl = returnUrl
            });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await _userManager.FindByNameAsync(model.UserName);

            if (user == default)
            {
                ModelState.AddModelError("UserName", "User not found");
                return View(model);
            }

            var signInResult = await _signInManager.PasswordSignInAsync(user, model.Password, false, false);

            if (signInResult.Succeeded)
            {
                return Redirect(model.ReturnUrl);
            }

            ModelState.AddModelError("UserName", "fatal error. launching nuclear missile");
            return View();
        }

        public async Task<IActionResult> LogOut(string logoutId)
        {
            await _signInManager.SignOutAsync();
            var logoutContext = await _interactionService.GetLogoutContextAsync(logoutId);
            if (logoutContext.PostLogoutRedirectUri.IsNullOrEmpty())
            {
                return RedirectToAction("Login");
            }

            return Redirect(logoutContext.PostLogoutRedirectUri);
        }
    }
}