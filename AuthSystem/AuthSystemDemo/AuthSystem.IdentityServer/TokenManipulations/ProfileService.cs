﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AuthSystem.DataModel.Auth;
using AuthSystem.DataModel.User;
using AuthSystem.IdentityServer.DAL;
using IdentityServer4.Models;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Identity;

namespace AuthSystem.IdentityServer.TokenManipulations
{
    public class ProfileService : IProfileService
    {
        private readonly UserManager<AuthSystemUser> _userManager;
        private readonly IdentityServerDbContext _identityServerDbContext;

        public ProfileService(UserManager<AuthSystemUser> userManager, IdentityServerDbContext identityServerDbContext)
        {
            _userManager = userManager;
            _identityServerDbContext = identityServerDbContext;
        }

        public Task GetProfileDataAsync(ProfileDataRequestContext context)
        {
            var id = context.Subject.Claims.FirstOrDefault(c => c.Type == "sub");
            if (id != default)
            {
                var user = _userManager.FindByIdAsync(id.Value).Result;
                //var company = _identityServerDbContext.Companies.FirstOrDefault(c => c.Id == user.CompanyId);
                var claims = _userManager.GetClaimsAsync(user).Result.Where(c => c.Type == ClaimTypes.Role);
                context.IssuedClaims.AddRange(claims);
            }

            return Task.CompletedTask;
        }

        public Task IsActiveAsync(IsActiveContext context)
        {
            context.IsActive = true;
            return Task.CompletedTask;
        }
    }
}