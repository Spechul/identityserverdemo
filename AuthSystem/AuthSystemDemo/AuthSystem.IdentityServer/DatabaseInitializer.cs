using System;
using System.Linq;
using System.Security.Claims;
using AuthSystem.DataModel.Auth;
using AuthSystem.DataModel.User;
using AuthSystem.IdentityServer.DAL;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;

namespace AuthSystem.IdentityServer
{
    public static class DatabaseInitializer
    {
        public static void Init(IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetService<UserManager<AuthSystemUser>>();
            var dbContext = serviceProvider.GetService<IdentityServerDbContext>();
            if (userManager.Users.FirstOrDefault() != default)
            {
                return;
            }

            dbContext.Companies.RemoveRange(dbContext.Companies);

            var company1 = new Company()
            {
                Id = new Guid("DB2202C3-4C37-4CEB-9F1E-0D54D764935D"),
                Name = "UnitedCompany"
            };

            dbContext.Companies.Add(company1);

            var testUser = new AuthSystemUser()
            {
                UserName = "qwe",
                CompanyId = company1.Id
            };

            //userManager.DeleteAsync(testUser).Wait();

            userManager.CreateAsync(testUser, "asd").Wait();

            var company2 = new Company()
            {
                Id = new Guid("DB2879FC-D5EF-463C-84A6-483A8F4597A5"),
                Name = "Shell"
            };

            dbContext.Companies.Add(company2);
            dbContext.SaveChanges();

            var adminUser = new AuthSystemUser()
            {
                UserName = "admin",
                CompanyId = company2.Id
            };

            //userManager.DeleteAsync(adminUser).Wait();

            var adminResult = userManager.CreateAsync(adminUser, "admin").Result;

            if (adminResult.Succeeded)
            {
                userManager.AddClaimAsync(adminUser, new Claim(ClaimTypes.Role, AuthConstants.Roles.Administrator)).Wait();
            }
        }
    }
}