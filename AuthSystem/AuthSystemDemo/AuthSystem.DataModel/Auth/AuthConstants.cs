﻿namespace AuthSystem.DataModel.Auth
{
    public static class AuthConstants
    {
        public static class Roles
        {
            public const string Administrator = "Administrator";
        }

        public static class ApiScopes
        {
            public const string ClientMvc = "ClientMvc";
        }

        public static class ApiResources
        {
            public const string Measurements = "Measurements";
            public const string ApiOne = "ApiOne";
        }
    }
}