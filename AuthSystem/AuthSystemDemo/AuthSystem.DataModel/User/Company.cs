﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace AuthSystem.DataModel.User
{
    public class Company
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<AuthSystemUser> Users { get; set; }
    }
}