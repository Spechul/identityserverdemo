﻿using System;
using Microsoft.AspNetCore.Identity;

namespace AuthSystem.DataModel.User
{
    public class AuthSystemUser : IdentityUser<Guid>
    {
        public Guid CompanyId { get; set; }
    }
}