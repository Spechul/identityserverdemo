﻿using System;

namespace AuthSystem.DataModel.Data
{
    public class Measurement
    {
        private static Random _random = new Random();

        public double Value { get; set; }
        public DateTime TimeStamp { get; set; }

        public static Measurement RandomMeasurement()
        {
            var measurement = new Measurement()
            {
                Value = _random.NextDouble(),
                TimeStamp = DateTime.UtcNow
            };
            return measurement;
        }
    }
}