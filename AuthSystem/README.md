# IdentityServer4 4.x demo


## Projects

- AuthSystem.ApiOne - api project with 2 \[Authorize\] secured routes: 
  - /Secret/String
  - /Secret/Admin (requires Role claim to be set to "Administrator").
- AuthSystem.DataModel - objects library.
- AuthSystem.GrpcClient - console app for making Greeter.SayHello request using `client credentials` grant type flow.
- AuthSystem.GrpcService - Basic GreeterService (from ms gRPC project snippet), secured with \[Authorize\].
- AuthSystem.IdentityServer - IdentityServer4 4.x itself.
- AuthSystem.IdentityServer.DAL - data access layer with overriten IdentityUser and IdentityRole.
- AuthSystem.MvcClient - mvc client with `code` grant type flow.
- AuthSystem.SpaClient - JavaScript client with `implicit` grant type flow.
- SpaClient - JavaScript client with `code` grant type flow.

## How to run

Run 
```
docker-compose up 
```
on ... /IdentityServiceDemo/docker/AuthSystem
or get postgres up any other way on your `localhost` machine with port 5432.

Run 
```
Update-Database
```
or 
```
dotnet ef update
```
on AuthSystem.IdentityServer.DAL lib to setup database migrations for IdentityServer. IdentityServer would populate database itself on first run.

### MvcClient
Set those projects as startup:
- AuthSystem.IdentityServer
- AuthSystem.MvcClient

### SpaClient / AuthSystem.SpaClient
Set those projects as startup:
- AuthSystem.IdentityServer
- AuthSystem.ApiOne
- SpaClient or AuthSystem.SpaClient

### GrpcClient
Set those projects as startup:
- AuthSystem.IdentityServer
- AuthSystem.GrpcClient
- AuthSystem.GrpcService